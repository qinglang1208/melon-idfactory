package com.fetech.melon.idfactory.utils;

import java.text.ParseException;
import java.util.Date;

import com.fetech.melon.idfactory.exception.UidGenerateException;
import org.apache.commons.lang.time.DateFormatUtils;

/**
 * DateUtils provides date formatting, parsing
 *
 * @author yutianbao
 */
public abstract class MelonDateUtils extends org.apache.commons.lang.time.DateUtils {
    /**
     * Patterns
     */
    private static final String DAY_PATTERN = "yyyy-MM-dd";
    private static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    //public static final String DATETIME_MS_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";

    //public static final Date DEFAULT_DATE = DateUtils.parseByDayPattern("1970-01-01");

    /**
     * Parse date by 'yyyy-MM-dd' pattern
     */
    public static Date parseByDayPattern(String str) {
        return parseDate(str, DAY_PATTERN);
    }

    /*
     * Parse date by 'yyyy-MM-dd HH:mm:ss' pattern

    public static Date parseByDateTimePattern(String str) {
        return parseDate(str, DATETIME_PATTERN);
    }

     */

    /**
     * Parse date without Checked exception
     *
     * @throws RuntimeException when ParseException occurred
     */
    private static Date parseDate(String str, String pattern) {
        try {
            return parseDate(str, new String[]{pattern});
        } catch (ParseException e) {
            throw new UidGenerateException(e);
        }
    }

    /*
     * Format date into string

    public static String formatDate(Date date, String pattern) {
        return DateFormatUtils.format(date, pattern);
    }


    /*
     * Format date by 'yyyy-MM-dd' pattern

    private static String formatByDayPattern(Date date) {
        if (date != null) {
            return DateFormatUtils.format(date, DAY_PATTERN);
        } else {
            return null;
        }
    }
     */

    /**
     * Format date by 'yyyy-MM-dd HH:mm:ss' pattern
     */
    public static String formatByDateTimePattern(Date date) {
        return DateFormatUtils.format(date, DATETIME_PATTERN);
    }

    /*
     * Get current day using format date by 'yyyy-MM-dd HH:mm:ss' pattern

    public static String getCurrentDayByDayPattern() {
        Calendar cal = Calendar.getInstance();
        return formatByDayPattern(cal.getTime());
    }

     */
}
