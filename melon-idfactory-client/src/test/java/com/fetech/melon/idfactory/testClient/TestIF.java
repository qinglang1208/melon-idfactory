package com.fetech.melon.idfactory.testClient;

import com.fetech.melon.context.log.LogUtil;
import com.fetech.melon.idfacotry.client.IdFactoryClient;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by ZhangGang on 2017/10/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:META-INF/spring/spring-test-idfactory-client.xml"})
public class TestIF {

    @Resource
    private IdFactoryClient idFactoryClient;

    @Test
    public void uuid() {
        LogUtil.debug("uuid:");
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getUUID());
        }
    }

    @Test
    public void uid() {
        LogUtil.debug("64 uid:");
        for (int i = 0; i < 10; i++) {
            long id = idFactoryClient.get64Uid();
            LogUtil.debug(id + "");
            LogUtil.debug(idFactoryClient.parse64Uid(id));
        }
    }

    @Test
    public void incrementId() {
        LogUtil.debug("auto increment id:");
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getIncrementId("test_1") + "");
        }
    }

    @Test
    public void incrementId2() {
        LogUtil.debug("auto increment2 id:");
        boolean ret = idFactoryClient.initIncrementId("test_2", 10);
        Assert.assertTrue(ret);
        for (int i = 0; i < 10; i++) {
            LogUtil.debug(idFactoryClient.getIncrementId("test_2") + "");
        }
    }
}
