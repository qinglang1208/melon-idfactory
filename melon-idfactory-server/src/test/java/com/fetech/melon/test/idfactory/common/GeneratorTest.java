package com.fetech.melon.test.idfactory.common;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:melon-test-uid.xml"})
public abstract class GeneratorTest {

    protected static final boolean VERBOSE = true;
    private static final int THREADS = Runtime.getRuntime().availableProcessors() << 1;

    /**
     * Test for parallel generate
     */
    @Test
    public void testParallelGenerate() throws InterruptedException, IOException {
        if (getSize() > 0) {
            AtomicInteger control = new AtomicInteger(-1);
            Set<Long> uidSet = new ConcurrentSkipListSet<>();

            // Initialize threads
            List<Thread> threadList = new ArrayList<>(THREADS);
            for (int i = 0; i < THREADS; i++) {
                Thread thread = new Thread(() -> workerRun(uidSet, control));
                thread.setName("UID-generator-" + i);

                threadList.add(thread);
                thread.start();
            }

            // Wait for worker done
            for (Thread thread : threadList) {
                thread.join();
            }

            // Check generate 10w times
            Assert.assertEquals(getSize(), control.get());

            // Check UIDs are all unique
            checkUniqueID(uidSet);
        }
    }

    public void checkUniqueID(Set<Long> uidSet) throws IOException {

    }

    public void workerRun(Set<Long> uidSet, AtomicInteger control) {

    }

    public int getSize() {
        return 0;
    }

}
