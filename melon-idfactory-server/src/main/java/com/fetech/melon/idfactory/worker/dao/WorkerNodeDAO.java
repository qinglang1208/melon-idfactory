package com.fetech.melon.idfactory.worker.dao;

import com.fetech.melon.idfactory.worker.entity.WorkerNodeEntity;

/**
 * DAO for M_WORKER_NODE
 *
 * @author yutianbao
 */
public interface WorkerNodeDAO {

    //WorkerNodeEntity getWorkerNodeByHostPort(String host, String port);

    void addWorkerNode(WorkerNodeEntity workerNodeEntity);

}
