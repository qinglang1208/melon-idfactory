package com.fetech.melon.idfactory.rpc;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by ZhangGang on 2017/9/29.
 */
@Path("/idfactory")
public interface IdFactory {

    @GET
    @Path("/getIncrementId/{key}")
    @Produces(MediaType.TEXT_PLAIN)
    long getIncrementId(@PathParam("key") String key);

    @POST
    @Path("/initIncrementId/{key}/{start}")
    void initIncrementId(@PathParam("key") String key, @PathParam("start") long start);

    @GET
    @Path("/getUUID")
    String getUUID();

    @GET
    @Path("/get64Uid")
    @Produces(MediaType.TEXT_PLAIN)
    long get64Uid();

    @GET
    @Path("/parse64Uid/{uid}")
    @Produces(MediaType.APPLICATION_JSON)
    String parse64Uid(@PathParam("uid") long uid);

}
